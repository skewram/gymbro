import React, {Button, Text, View} from 'react-native';

function HomeScreen({navigation}): JSX.Element {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home screen</Text>
      <Button
        title="Go scanner des trucs"
        onPress={() => navigation.navigate('Barcode')}
      />
    </View>
  );
}

export default HomeScreen;
