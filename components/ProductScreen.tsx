import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useFocusEffect} from '@react-navigation/native';
import {useCallback, useState} from 'react';
import React, {
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Product from '../utils/Product';
import ProgressBar from './ProgressBar';

function ProductScreen({route, navigation}): JSX.Element {
  const {barcode} = route.params;
  const [data, setData] = useState<Product>();
  const [quantity, setQuantity] = useState(100);

  const addItem = () => {
    navigation.navigate('Accueil');
    navigation.popToTop();
  };

  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      fetch(`https://world.openfoodfacts.org/api/v2/product/${barcode}`).then(
        async res => {
          setData(Product.parseProduct(await res.json()));
        },
      );
      return () => {};
    }, []),
  );

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <ScrollView>
        <View style={styles.view}>
          {data?.image ? (
            <Image
              source={{
                uri: data?.image,
              }}
              style={styles.productImage}
            />
          ) : (
            <View style={styles.imageReplacer} />
          )}
          <Text style={styles.productLabel}>{data?.name}</Text>
          <View style={styles.separator} />
          <Text style={styles.energy}>
            {Math.floor(((data?.energy ?? 0) * quantity) / 100)}KCal
          </Text>
          <ProgressBar
            label="Protéines"
            color="#2C96F9"
            value={
              data?.nutriments.proteins
                ? (data?.nutriments.proteins * quantity) / 100 / (1.2 * 70)
                : 0
            }
          />
          <ProgressBar
            label="Glucides"
            color="#F92C82"
            value={
              data?.nutriments.glucide
                ? (data?.nutriments.glucide * quantity) / 100 / (1 * 70)
                : 0
            }
          />
          <ProgressBar
            label="Fibres"
            color="#2C96F9"
            value={
              data?.nutriments.fiber
                ? (data?.nutriments.fiber * quantity) / 100 / 30
                : 0
            }
          />
          <View style={{flexDirection: 'row'}}>
            <TextInput
              placeholder="Quantité en grammes"
              value={quantity.toString()}
              keyboardType="number-pad"
              onChangeText={t => {
                try {
                  if (t.length > 0) {
                    setQuantity(parseFloat(t));
                  } else {
                    setQuantity(0);
                  }
                } catch {
                  setQuantity(0);
                }
              }}
              style={{backgroundColor: 'white'}}
            />
            <Pressable style={{backgroundColor: '#282828'}} onPress={addItem}>
              <MaterialIcons name="plus" size={54} color="#FFFFFF" />
            </Pressable>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  view: {
    flex: 1,
    alignItems: 'center',
    marginTop: 24,
    paddingHorizontal: 8,
  },
  energy: {
    marginVertical: 24,
    fontSize: 48,
    fontWeight: 'bold',
    color: '#000000',
  },
  imageReplacer: {
    backgroundColor: '#BBBBBB',
    height: 192,
    width: 192,
    borderRadius: 25,
  },
  productImage: {
    height: 192,
    width: 192,
    borderRadius: 25,
  },
  productLabel: {
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 32,
    color: '#000',
    alignSelf: 'flex-start',
  },
  separator: {
    width: 252,
    height: 4,
    borderRadius: 25,
    backgroundColor: '#282828',
    alignSelf: 'flex-start',
    marginVertical: 16,
  },
});

export default ProductScreen;
