/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import {useFocusEffect} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useCallback, useEffect, useState} from 'react';
import {Alert, StyleSheet, ToastAndroid, View} from 'react-native';
import {runOnJS} from 'react-native-reanimated';

import {
  Camera,
  useCameraDevices,
  useFrameProcessor,
} from 'react-native-vision-camera';
import {Barcode, scanBarcodes} from 'vision-camera-code-scanner';
import {
  BARCODES_FORMAT_SUPPORTED,
  CAMERA_REFUSE_MESSAGE,
  CAMERA_REFUSE_TITLE,
  PRODUCT_DETECTED_TOAST_MESSAGE,
} from '../Constants';
import ProductScreen from './ProductScreen';

function ScannerScreen({navigation}): JSX.Element {
  const [barcodeDetectionEnabled, setBarcodeDetectionEnabled] = useState(false);
  const [barcodes, setBarcodes] = useState<Barcode[]>([]);

  const devices = useCameraDevices();
  const frameProcessor = useFrameProcessor(frame => {
    'worklet';
    const detectedBarcodes = scanBarcodes(frame, BARCODES_FORMAT_SUPPORTED, {
      checkInverted: false,
    });
    runOnJS(setBarcodes)(detectedBarcodes);
  }, []);

  useEffect(() => {
    if (barcodes.length === 1 && barcodeDetectionEnabled === true) {
      const code = barcodes.pop()?.rawValue ?? '';
      navigation.push('Product', {
        barcode: code,
      });
      ToastAndroid.show(PRODUCT_DETECTED_TOAST_MESSAGE, ToastAndroid.SHORT);
    }
  }, [barcodes, barcodeDetectionEnabled]);

  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      Camera.getCameraPermissionStatus()
        .then(async cameraPermission => {
          if (cameraPermission !== 'authorized') {
            const result = await Camera.requestCameraPermission();
            if (result === 'denied') {
              Alert.alert(CAMERA_REFUSE_TITLE, CAMERA_REFUSE_MESSAGE);
              navigation.goBack();
              return;
            }
          }
          setBarcodeDetectionEnabled(true);
        })
        .catch(() => {
          navigation.goBack();
        });

      return () => {
        setBarcodeDetectionEnabled(false);
      };
    }, []),
  );

  return (
    <View style={styles.page}>
      {barcodeDetectionEnabled && devices.back ? (
        <Camera
          device={devices.back}
          isActive={true}
          style={StyleSheet.absoluteFill}
          frameProcessor={frameProcessor}
          frameProcessorFps={30}
        />
      ) : null}
      <View style={styles.barcodeOverlay} />
    </View>
  );
}

function BarcodeScreen({navigation}): JSX.Element {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Root" component={ScannerScreen} />
      <Stack.Screen name="Product" component={ProductScreen} />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  barcodeOverlay: {
    height: 100,
    width: 300,
    borderRadius: 25,
    borderStyle: 'solid',
    borderColor: '#bdbdbd',
    borderWidth: 5,
    marginBottom: 18,
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
});

export default BarcodeScreen;
