import React, {Button, Text, View} from 'react-native';
import * as Progress from 'react-native-progress';

function ProgressBar({label, value, color}) {
  return (
    <View style={{flexDirection: 'column'}}>
      <Text>{label}</Text>
      <Progress.Bar
        progress={value}
        unfilledColor="#D9D9D9"
        color={color}
        borderWidth={0}
        width={296}
        borderRadius={25}
        height={12}
        style={{marginVertical: 8}}
      />
    </View>
  );
}

export default ProgressBar;
