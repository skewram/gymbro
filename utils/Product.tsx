export default class Product {
  name;
  energy;
  image;
  nutriments;

  constructor(data: any) {
    this.name = data.generic_name_fr ?? data.generic_name;
    this.energy = parseInt(data.nutriments['energy-kcal_100g'], 10) ?? 0;
    this.image = data.image_front_url;
    this.nutriments = {
      calcium: parseFloat(data.nutriments.calcium_100g) ?? 0,
      glucide: parseFloat(data.nutriments.carbohydrates_100g) ?? 0,
      fat: parseFloat(data.nutriments.fat_100g) ?? 0,
      fiber: parseFloat(data.nutriments.fiber_100g) ?? 0,
      iron: parseFloat(data.nutriments.iron_prepared_100g) ?? 0,
      phosphore: parseFloat(data.nutriments.phosphorus_prepared_100g) ?? 0,
      potassium: parseFloat(data.nutriments.potassium_prepared_100g) ?? 0,
      proteins: parseFloat(data.nutriments.proteins_100g) ?? 0,
      salt: parseFloat(data.nutriments.salt_100g) ?? 0,
      saturated_fat: parseFloat(data.nutriments['saturated-fat_100g']) ?? 0,
      sodium: parseFloat(data.nutriments.sodium_100g) ?? 0,
      sugar: parseFloat(data.nutriments.sugars_prepared_100g) ?? 0,
      vitamin_b1: parseFloat(data.nutriments['vitamin-b1_prepared_100g']) ?? 0,
      vitamin_c: parseFloat(data.nutriments['vitamin-c_100g']) ?? 0,
      vitamin_d: parseFloat(data.nutriments['vitamin-d_100g']) ?? 0,
      zinc: parseFloat(data.nutriments.zinc_prepared_100g) ?? 0,
    };
  }

  static parseProduct(data: any) {
    return new Product(data.product);
  }
}
