import {BarcodeFormat} from 'vision-camera-code-scanner';

export const BARCODES_FORMAT_SUPPORTED = [BarcodeFormat.ALL_FORMATS];
export const CAMERA_REFUSE_TITLE = 'Permission';
export const CAMERA_REFUSE_MESSAGE =
  "Le scan du code barre nécessite l'accès temporaire à votre caméra";
export const PRODUCT_DETECTED_TOAST_MESSAGE = 'Produit détecté';

export const COLORS = {
  THEME: {
    LIGHT: {},
    DARK: {},
  },
};
