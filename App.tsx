/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import BarcodeScreen from './components/BarcodeScreen';
import HomeScreen from './components/HomeScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createBottomTabNavigator();

function MenuIcon(iconName: string, size: number, color: string) {
  return <MaterialIcons name={iconName} size={size} color={color} />;
}

function App(): JSX.Element {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Accueil"
        screenOptions={({route}) => ({
          tabBarIcon: ({color, size}) => {
            switch (route.name) {
              case 'Accueil':
                return MenuIcon('home', size, color);
              case 'Barcode':
                return MenuIcon('barcode-scan', size, color);
              case 'Journal':
                return MenuIcon('book-open-variant', size, color);
            }
          },
          headerShown: false,
        })}>
        <Tab.Screen name="Accueil" component={HomeScreen} />
        <Tab.Screen name="Journal" component={HomeScreen} />
        <Tab.Screen name="Barcode" component={BarcodeScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
