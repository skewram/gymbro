export const GENDER = {
    MALE: "male",
    FEMALE: "female"
}
export const ACTIVITY_LEVEL = {
    NOT_ACTIF: "not_actif",
    BIT_ACTIF: "bit_actif",
    ACTIF: "actif",
    VERY_ACTIF: "very_actif"
}
