export default class Product {
  constructor(data) {
    console.log(data);
    this.name = data.abbreviated_product_name_fr;
    this.energy = parseInt(data.nutriments["energy-kcal_100g"]);
    this.nutriments = {
      calcium: parseFloat(data.nutriments.calcium_100g),
      glucide: parseFloat(data.nutriments.carbohydrates_100g),
      fat: parseFloat(data.nutriments.fat_100g),
      fiber: parseFloat(data.nutriments.fiber_100g),
      iron: parseFloat(data.nutriments.iron_prepared_100g),
      phosphore: parseFloat(data.nutriments.phosphorus_prepared_100g),
      potassium: parseFloat(data.nutriments.potassium_prepared_100g),
      proteins: parseFloat(data.nutriments.proteins_100g),
      salt: parseFloat(data.nutriments.salt_100g),
      saturated_fat: parseFloat(data.nutriments["saturated-fat_100g"]),
      sodium: parseFloat(data.nutriments.sodium_100g),
      sugar: parseFloat(data.nutriments.sugars_prepared_100g),
      vitamin_b1: parseFloat(data.nutriments["vitamin-b1_prepared_100g"]),
      vitamin_c: parseFloat(data.nutriments["vitamin-c_100g"]),
      vitamin_d: parseFloat(data.nutriments["vitamin-d_100g"]),
      zinc: parseFloat(data.nutriments["zinc_prepared_100g"]),
    };
  }

  static parseProduct(data) {
    return new Product(data.product);
  }
}
