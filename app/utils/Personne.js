export default class Personne {
    metabolism
    /**
     * Instantiate a new Personne
     * @param {'male' | 'female'} gender Gender of the personne
     * @param {number} age Age in years
     * @param {number} weight Weight in kilogramms
     * @param {number} height Height in meters
     */
    constructor(gender, age, weight, height) {
        const coefA = gender === 'male' ? 13.707 : 9.740
        const coefB = gender === 'male' ? 492.3 : 172.9
        const coefC = gender === 'male' ? 6.673 : 4.737
        const coefD = gender === 'male' ? 77.607 : 667.051
        this.metabolism = coefA * weight + coefB * height - coefC * age + coefD
    }
}
